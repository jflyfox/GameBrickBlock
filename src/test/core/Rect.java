package test.core;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import com.flyfox.game.core.WObject;

public class Rect extends WObject {
	public Rect(double x, double y, double width, double height) {
		super(x, y, width, height);
	}

	@Override
	public void draw(GraphicsContext gc) {
		gc.setFill(Color.CHOCOLATE);
		gc.fillRect(getX(), getY(), getWidth(), getHeight());
	}

	@Override
	public void update() {
		moveX(1);
	}
}
