package test.core;

import javafx.scene.input.KeyEvent;

import com.flyfox.game.core.WScreen;
import com.flyfox.game.core.WSystem;

public class TestScreen extends WScreen {
    private Rect player;
	public TestScreen(double width, double height) {
		super(width, height);
		
		player = new Rect(50, 50, 100, 100);
		addObject(player);
	}

	@Override
	protected void onKeyPressed(KeyEvent event) {
       switch (event.getCode()) {
	case UP:
		player.moveY(-10);
		break;
	case DOWN:
		player.moveY(10);
		break;
	case ENTER:
		addObject(new Rect(Math.random() * WSystem.WIDTH, Math.random() * WSystem.HEIGHT, 100, 100));
		break;
	default:
		break;
	}
	}

	@Override
	protected void onKeyReleased(KeyEvent event) {

	}

}
