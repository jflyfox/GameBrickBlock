package test.core;


import javafx.scene.paint.Color;
import javafx.stage.Stage;

import com.flyfox.game.core.WApplication;
import com.flyfox.game.core.WSystem;

public class MainClass extends WApplication {

	@Override
	protected void loadBefore() {
		setWindowSize(800, 600);
	}

	@Override
	protected void loadEnd() {		
		TestScreen testScreen = new TestScreen(WSystem.WIDTH, WSystem.HEIGHT);
		getRoot().getChildren().add(testScreen);
		testScreen.start();
		testScreen.initEvents();
		
		getScene().setFill(Color.BLACK);
	}
	
	@Override
	protected void showStage(Stage stage){
		super.showStage(stage);
		stage.setTitle("JavaFX游戏开发 第二课 基础游戏框架");
	}
	public static void main(String[] args) {
		launch(args);
	}
}
