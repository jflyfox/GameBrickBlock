package test;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.BoxBlur;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import com.flyfox.game.core.WSystem;

/**
 * test clearRect Effect , failed !!!
 * 
 */
public class TestClass extends Application {

	private Group mGroup;
	private Scene mScene;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		Canvas canvas = new Canvas(WSystem.WIDTH, WSystem.HEIGHT);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		gc.setFill(Color.YELLOW);
		BoxBlur mBlur = new BoxBlur();
		mBlur.setWidth(5);
		mBlur.setHeight(5);
		gc.setEffect(mBlur);
		gc.fillRoundRect(20, 20, 200, 25, 20, 20);
		
		gc.setEffect(null);
		gc.clearRect(0, 0, WSystem.WIDTH, WSystem.HEIGHT);
		
		
		mGroup = new Group();
		mGroup.getChildren().add(canvas);
		mScene = new Scene(mGroup, WSystem.WIDTH, WSystem.HEIGHT);

		mScene.setFill(Color.BLACK);
		stage.setScene(mScene);
		stage.show();

	}
}
