package com.flyfox.game.brickblock;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.InnerShadow;
import javafx.scene.paint.Color;

import com.flyfox.game.core.WObject;

public class Brick extends WObject {

	private int hp;
	private double arcWidth = 1;
	private double arcHeight = 1;
	private Color color = Color.YELLOW;

	public Brick(Color color, int hp) {
		this.hp = hp;
		this.color = color;

		setWidth(100);
		setHeight(25);
	}

	public int getHp() {
		return this.hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	@Override
	public void draw(GraphicsContext gc) {
		gc.setFill(color);
		
		
		InnerShadow is = new InnerShadow();
		is.setOffsetX(-4.0f);
		is.setOffsetY(-4.0f);
		gc.setEffect(is);
		gc.setStroke(Color.BLACK);
		//gc.strokeRoundRect(getX(), getY(), getWidth(), getHeight(), arcWidth, arcHeight);
		gc.fillRoundRect(getX(), getY(), getWidth(), getHeight(), arcWidth, arcHeight);

	}

	@Override
	public void update() {
		// TODO Auto-generated method stub

	}

}
