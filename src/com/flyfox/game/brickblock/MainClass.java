package com.flyfox.game.brickblock;

import javafx.scene.paint.Color;
import javafx.stage.Stage;

import com.flyfox.game.core.WApplication;
import com.flyfox.game.core.WSystem;

public class MainClass extends WApplication {

	@Override
	protected void loadBefore() {
		setWindowSize(WSystem.WIDTH, WSystem.HEIGHT);
	}

	@Override
	protected void loadEnd() {
		GameScreen testScreen = new GameScreen(WSystem.WIDTH, WSystem.HEIGHT);
		getRoot().getChildren().add(testScreen);
		testScreen.start();
		testScreen.initEvents();

		getScene().setFill(Color.BLACK);
	}

	@Override
	protected void showStage(Stage stage) {
		super.showStage(stage);
		stage.setTitle("JavaFX游戏开发 打砖块");
	}

	public static void main(String[] args) {
		launch(args);
	}
}
