package com.flyfox.game.brickblock;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.Glow;
import javafx.scene.paint.Color;

import com.flyfox.game.core.WObject;
import com.flyfox.game.core.WSystem;

/**
 * @author wing
 * @date 2012/7/26
 */
public class Ball extends WObject {
	private int speedX = 2;
	private int speedY = -2;

	public Ball(int centerX, int centerY, int radius) {
		setWidth(2 * radius);
		setHeight(2 * radius);
		setX(centerX);
		setY(centerY);
	}

	public void setSpeedX(int speedX) {
		this.speedX = speedX;
	}

	public int getSpeedX() {
		return speedX;
	}

	public void setSpeedY(int speedY) {
		this.speedY = speedY;
	}

	public int getSpeedY() {
		return speedY;
	}

	@Override
	public void draw(GraphicsContext gc) {
		gc.setFill(Color.LIGHTBLUE);
		gc.setEffect(new Glow());
		gc.fillOval(getX(), getY(), getWidth(), getHeight());
	}

	@Override
	public void update() {
		this.moveX(this.getSpeedX());
		this.moveY(this.getSpeedY());

		if ((this.getX() <= 0.0D) || (this.getX() >= WSystem.WIDTH - this.getWidth())) {
			this.setSpeedX(-this.getSpeedX());
		}

		if ((this.getY() <= 0.0D) || (this.getY() >= WSystem.HEIGHT - this.getHeight())) {
			this.setSpeedY(-this.getSpeedY());
		}

	}

}