package com.flyfox.game.brickblock;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.BoxBlur;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import com.flyfox.game.core.WObject;
import com.flyfox.game.core.WSystem;

public class BottomBrick extends WObject {

	private double arcWidth = 20;
	private double arcHeight = 20;
	private Color color = Color.YELLOW;

	public BottomBrick() {
		setWidth(180);
		setHeight(25);
		setX(0);
		setY(WSystem.HEIGHT - getHeight());
	}

	public void onMouseMove(MouseEvent event) {
		if (event.getX() >= getWidth() / 2 //
				&& event.getX() <= WSystem.WIDTH - getWidth() / 2) {
			setX(event.getX() - getWidth() / 2);
		}
	}

	@Override
	public void draw(GraphicsContext gc) {
		gc.setFill(color);
		BoxBlur mBlur = new BoxBlur();
		mBlur.setWidth(5);
		mBlur.setHeight(5);
		gc.setEffect(mBlur);
		gc.fillRoundRect(getX(), getY(), getWidth(), getHeight(), arcWidth, arcHeight);
	}

	@Override
	public void update() {
		// moveX(1);
	}

	public double getArcWidth() {
		return arcWidth;
	}

	public void setArcWidth(double arcWidth) {
		this.arcWidth = arcWidth;
	}

	public double getArcHeight() {
		return arcHeight;
	}

	public void setArcHeight(double arcHeight) {
		this.arcHeight = arcHeight;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

}
