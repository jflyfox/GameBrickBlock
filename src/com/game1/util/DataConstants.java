package com.game1.util;
import javafx.scene.image.Image;

public class DataConstants {

	public static Image getMap() {
		return getImage("/image/map0.png");
	}
	
	public static Image getEnemy() {
		return getImage("/image/enemy1.png");
	}
	
	public static Image getPlayer1() {
		return getImage("/image/player1.png");
	}
	
	public static Image getPlayer2() {
		return getImage("/image/player2.png");
	}
	
	public static Image getPlayer3() {
		return getImage("/image/player3.png");
	}

	private static Image getImage(String image) {
		return new Image(DataConstants.class.getResourceAsStream(image));
	}
}
